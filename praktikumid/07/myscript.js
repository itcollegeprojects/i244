window.onload = function() {
	var elements = document.getElementsByClassName('autoResizeImage');
	for (var i = 0; i < elements.length; i++) {
		elements[i].addEventListener('click', function() {showDetails(this)
		}, false);
	}
}

function showDetails(p) {	
	if (!document.getElementById("hoidja")) {
	    return;
	}
	
	s = document.getElementById("suurpilt");
	var tmp = p.src;
	tmp = tmp.replace("/thumbs/","/img/").replace(".resized","");
	s.src = tmp;
	
	s.addEventListener('onload', function() {suurus(p)}, false);
	
	el = document.getElementById("hoidja")
	el.style.display="inline";
}

function hideDetails() {
	el = document.getElementById("hoidja")
	el.style.display="none";
}

function suurus(el){
	  el.removeAttribute("height"); // eemaldab suuruse
	  el.removeAttribute("width");
	  if (el.width>window.innerWidth || el.height>window.innerHeight){  // ainult liiga suure pildi korral
	    if (window.innerWidth >= window.innerHeight){ // lai aken
	      el.height=window.innerHeight*0.75; // 90% kõrgune
	      if (el.width>window.innerWidth){ // kas element läheb ikka üle piiri?
	        el.removeAttribute("height");
	        el.width=window.innerWidth*0.75;
	      }
	    } else { // kitsas aken
	      el.width=window.innerWidth*0.75;   // 90% laiune
	      if (el.height>window.innerHeight){ // kas element läheb ikka üle piiri?
	        el.removeAttribute("width");
	        el.height=window.innerHeight*0.75;
	      }
	    }
	  }
	}

window.onresize=function() {
	suurpilt = document.getElementById("suurpilt");
	suurus(suurpilt);
}

function hasClass(element, cls) {
	// v6tsin siit: http://stackoverflow.com/questions/5898656/test-if-an-element-contains-a-class
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

