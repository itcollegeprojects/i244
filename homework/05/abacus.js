window.onload = function() {
	var elements = document.getElementsByClassName('bead');
	for (var i = 0; i < elements.length; i++) {
		elements[i].addEventListener('click', function() {moveBead(this)
		}, false);
	}
}

function moveBead(p) {	
	if (hasClass(p, 'left')) {
		p.classList.remove('left');
		p.classList.add('right');
	} else {
		p.classList.remove('right');
		p.classList.add("left");
	}
}

function hasClass(element, cls) {
	// v6tsin siit: http://stackoverflow.com/questions/5898656/test-if-an-element-contains-a-class
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

